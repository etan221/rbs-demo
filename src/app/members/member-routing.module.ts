import { NgModule } from "@angular/core";
// import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  // {
  //   path: "",
  //   redirectTo: "home",
  //   pathMatch: "full",
  // },
  {
    path: "dashboard",
    loadChildren: () =>
      import("./dashboard/dashboard.module").then((m) => m.DashboardPageModule),
  },
  // {
  //   path: "register",
  //   loadChildren: () =>
  //     import("./public/register/register.module").then(
  //       (m) => m.RegisterPageModule
  //     ),
  // },
];

@NgModule({
  // declarations: [],
  // imports: [CommonModule],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MemberRoutingModule {}
