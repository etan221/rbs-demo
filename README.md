# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### start rbs-demo
- Building a Basic Ionic 4 Login Flow with Angular Router, [video](https://www.youtube.com/watch?v=z3pDqnuyzZ4), [text](https://devdactic.com/ionic-4-login-angular/)
- `ionic start rbs-demo blank --type=angular`
```
[INFO] Next Steps:

       - Go to your newly created project: cd .\rbs_demo
       - Run ionic serve within the app directory to see your app
       - Build features and components: https://ion.link/scaffolding-docs
       - Run your app on a hardware or virtual device: https://ion.link/running-docs
```
- `npm install --save @ionic/storage`
- delete folder: `home`
- `ionic g page public/login`
- `ionic g page public/register`
- `ionic g page members/dashboard`
- `ionic g service services/authentication`
- `ionic g service services/authGuard`
- `ng generate module members/member-routing --flat`
- `ionic serve`
- app.module.ts [IonicStorageModule.forRoot()](https://youtu.be/z3pDqnuyzZ4?t=319 )


- `ionic serve --port=8100`

- `ionic start rbs_demo tabs --type=angular --capacitor`
